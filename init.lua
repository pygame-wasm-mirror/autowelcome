-- Define a table to store the names and their corresponding timeouts
local name_list = {}

-- Function to insert a name into the list
function insert_name(name)
    -- Check if the name already exists in the list
    if name_list[name] then
        -- If the name already exists, update the timeout to 200 seconds
        name_list[name].timeout = 20
    else
        -- If the name doesn't exist, add it to the list with the specified timeout
        name_list[name] = {
            timeout = 20
        }
    end
end

-- Function to check if a name is in the list
function is_in_list(name)
    return name_list[name] ~= nil
end

-- Function to check for expired names and remove them from the list
function clean_list(dtime)
    -- Iterate through the list of names
    for name, data in pairs(name_list) do
        -- Check if the timeout has expired
        if data.timeout <= 0 then
            -- If the timeout has expired, remove the name from the list
            name_list[name] = nil
        else
            -- Decrement the timeout for the name
            data.timeout = data.timeout - dtime
        end
    end
end
function findAndGreetNewValue(origTable, newTable)
    -- Define the array of greetings
    local greetings = {"Hello ", "Hallo ", "Aloha", "Hi ", "Welcome "}

    -- Convert the original table to a set for quick lookup
    local origSet = {}
    for _, value in ipairs(origTable) do
        origSet[value] = true
        insert_name(value)
    end

    -- Find the first new value in the second table
    local newValue = nil
    for _, value in ipairs(newTable) do
        if not origSet[value] then
            newValue = value
            break
        end
    end

    -- If a new value was found, return it with a random greeting
    if newValue then
        local randomGreeting = greetings[math.random(#greetings)]
        
        if is_in_list(newValue) then 
        randomGreeting = "wb"
        end
        
        return randomGreeting .." ".. newValue
    end

    -- If no new value was found, return nil
    return nil
end
-- you need to modify src/lua_api/l_client lua to ignore restriction flags 
-- Seed the random number generator
local lpp = nil
local jj=nil
local function handlechat(message)
lppx=minetest.get_player_names()
if (lpp and lppx  and jj) then
pm=findAndGreetNewValue(lpp,lppx)
if (pm)then
minetest.send_chat_message(pm)
end
end
lpp=lppx
jj=1
end
--minetest.register_on_mods_loaded(handlejoin)
minetest.register_on_receiving_chat_message(handlechat)
minetest.register_globalstep(clean_list)
